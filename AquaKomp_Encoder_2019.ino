#define VER "AquaLED_261220"
#include <Wire.h>
#include <OneWire.h>
#include <EEPROM.h>
#include "GyverEncoder.h"
#include "LCD_1602_RUS.h"
#define DRIVER_VERSION 1// 0 - маркировка драйвера дисплея кончается на 4АТ, 1 - на 4Т
#define POWER 1 // 1-POSITIVE 0 - NEGATIVE


//#include <LiquidCrystal_I2C.h>
#include <Adafruit_PWMServoDriver.h>
Adafruit_PWMServoDriver pwm = Adafruit_PWMServoDriver(0x42);
OneWire ds(12); // Объект OneWire
OneWire ds_led(11);
#define DS3231_ADDRESS 0x68


float Temp_val;
float delta = 0;
int8_t x_temp = 20;

float Temp_val_led;
float delta_led = 0;
int8_t x_temp_led = 20;

byte flagDallRead;
byte flagDallRead_led;
byte data[12]; //для температуры
byte data_led[12]; //для температуры
byte addr[8];
byte addr_led[8];
#define CLK 2
#define DT 3
#define SW 4

/*#define ch_1_name "LED"
  #define ch_2_name "LED2"
  #define ch_3_name "MG"
  #define ch_4_name "CH"*/

#define ch_1_name "CH 1"
#define ch_2_name "CH 2"
#define ch_3_name "CH 3"
#define ch_4_name "CH 4"

///////////////////////////////////конфигурация подключения нагрузки
#define cool_pin 6
#define cool_led_pin 5
#define ch_1_pin 7
#define ch_2_pin 8
#define ch_3_pin 9
#define ch_4_pin 10
#define food_pin A0
#define co2_pin A1

#if (POWER)
/////// включение реле по плюсу
#define ON HIGH
#define OFF LOW
#define CO2_OFF 0
#define CO2_ONN 255
#else
/////// включение реле по минусу
#define ON LOW
#define OFF HIGH
#define CO2_OFF 255
#define CO2_ONN 0
#endif
///////////////////////////////////конфигурация подключения нагрузки
#define DELAY_MENU 200


Encoder enc1(CLK, DT, SW);

// -------- АВТОВЫБОР ОПРЕДЕЛЕНИЯ ДИСПЛЕЯ-------------
// Если кончается на 4Т - это 0х27. Если на 4АТ - 0х3f
#if (DRIVER_VERSION)
LCD_1602_RUS lcd(0x27, 16, 2);
#else
LCD_1602_RUS lcd(0x3f, 16, 2);
#endif
// -------- АВТОВЫБОР ОПРЕДЕЛЕНИЯ ДИСПЛЕЯ-------------

int8_t menu_val = 1;
int8_t menu_val_2 = 0;
int8_t co2[4] = {0, 0, 0, 0};
int8_t day_pause[4] = {0, 0, 0, 0};
int8_t food[5] = {0, 0, 0, 0, 0};
int8_t ch_1[48];
int8_t ch_2[48];
int8_t ch_3[48];
int8_t ch_4[48];
int8_t h, m, s;
int8_t hr;

//*******RGB*******///
int rgb[5]; //= {4095,4095,4095,4095,4095};
//8.10 30min 20.40 30min
int rgb_motion [6] = {5, 10, 35, 20, 40, 45};

int moon_motion [6];//={21,10,20,3,55,15};

long Day_time;

#define PWM_LR_MIN 0 //Если необходим ток покоя на LED - изменить эту константу 
// #define PWM_LR_MAX 255//Если необходимо ограничить максимальную яркость - уменьшить значение
#define PWM_LR_PIN A2 //Пин порта, где будет ШИМ LR

#define PWM_LG_MIN 0 //Если необходим ток покоя на LED - изменить эту константу 
// #define PWM_LG_MAX 255//Если необходимо ограничить максимальную яркость - уменьшить значение
#define PWM_LG_PIN A0 //Пин порта, где будет ШИМ LG 

#define PWM_LB_MIN 0 //Если необходим ток покоя на LED - изменить эту константу 
// #define PWM_LB_MAX 255 //Если необходимо ограничить максимальную яркость - уменьшить значение
#define PWM_LB_PIN A1 //Пин порта, где будет ШИМ LB 

#define PWM_LW_MIN 0 //Если необходим ток покоя на LED - изменить эту константу 
// #define PWM_LW_MAX 255//Если необходимо ограничить максимальную яркость - уменьшить значение
#define PWM_LW_PIN A4 //Пин порта, где будет ШИМ LW

#define PWM_LC_MIN 0 //Если необходим ток покоя на LED - изменить эту константу 
// #define PWM_LC_MAX 255//Если необходимо ограничить максимальную яркость - уменьшить значение
#define PWM_LC_PIN A3 //Пин порта, где будет ШИМ LC


#define mn_ul 60UL //Дополнительные константы для удобства 
#define hr_ul 3600UL //Отражают соответствующие количества секунд 
#define d_ul 86400UL

long sunrise_start;//= 11*hr+2*mn; //Начало восхода в 9 - 45
long sunrise_duration;// = 30*mn_ul; //Длительность восхода 30 минут
long sunset_start;//= 20*hr+07*mn; //начало заката в 21-15
long sunset_duration;// = 30*mn_ul; //Длительность заката 30 минут
long moonrise_start;//= 12*hr+05*mn ;//Начало луны в 9 - 45
long moonrise_duration;// = 10*mn_ul;//Длительность восхода
long moonset_start; //= 23*hr+00*mn;//Конец луны в 11
long moonset_duration;// = 10*mn_ul; //Длительность заката луны

long  LR;
long  LG;
long LB;
long LW;
long LC;
long moon;

int pwm_moon_b = 10;
int pwm_moon_r = 10;

byte rejim_dnya; // 0- ночь 1-восход 2-день 3-закат 4- восход луны 5- полнолуние 6 - закат луны 7- пауза
//*******RGB*******///



int lcd_on = 0;

double counter, lcd_count = 0;
byte flag = 0;


/*uint8_t bell[8]  = {0x4,0xe,0xe,0xe,0x1f,0x0,0x4};
  uint8_t note[8]  = {0x2,0x3,0x2,0xe,0x1e,0xc,0x0};
  uint8_t clock[8] = {0x0,0xe,0x15,0x17,0x11,0xe,0x0};
  uint8_t heart[8] = {0x0,0xa,0x1f,0x1f,0xe,0x4,0x0};
  uint8_t duck[8]  = {0x0,0xc,0x1d,0xf,0xf,0x6,0x0};
  uint8_t check[8] = {0x0,0x1,0x3,0x16,0x1c,0x8,0x0};
  uint8_t cross[8] = {0x0,0x1b,0xe,0x4,0xe,0x1b,0x0};
  uint8_t retarrow[8] = {  0x1,0x1,0x5,0x9,0x1f,0x8,0x4};*/

/*byte up[8] =
  {
  0b00100,
  0b01110,
  0b10101,
  0b00100,
  0b00100,
  0b00100,
  0b00100,
  0b00100,
  };
*/
/*byte down[8] =
  {
  0b00100,
  0b00100,
  0b00100,
  0b00100,
  0b00100,
  0b10101,
  0b01110,
  0b00100,
  };*/

byte co2_[8] =
{
  0b00011,
  0b00011,
  0b01100,
  0b01100,
  0b00011,
  0b00011,
  0b01100,
  0b01100,
};
/*byte MG_ch[8] = {
  0b000100,
  0b001110,
  0b011111,
  0b010101,
  0b001010,
  0b010101,
  0b001010,
  0b000000
  };*/

//uint8_t MG_ch[8]  = {0x4, 0xe, 0x1f, 0x0, 0xa, 0x0, 0x15};
uint8_t up[8]  = {0x4, 0xe, 0x15, 0x4, 0x4, 0x4, 0x4};
uint8_t play[8]  = {0x8, 0xc, 0xe, 0xf, 0xe, 0xc, 0x8}; ////play https://www.quinapalus.com/hd44780udg.html
uint8_t pause[8]  = {0x1b,0x1b,0x1b,0x1b,0x1b,0x1b,0x1b};
uint8_t zakat[8]  = { 0x0, 0x0, 0x1f, 0xe, 0x4, 0x0, 0x0};
uint8_t voshod[8]  = {0x0, 0x0, 0x4, 0xe, 0x1f, 0x0, 0x0};
uint8_t sun[8]  = {0x0, 0xe, 0x11, 0x15, 0x11, 0xe, 0x0};
uint8_t moon_znak[8]  = {0x0, 0xe, 0x1f, 0x1b, 0x1f, 0xe, 0x0};



void setup(void)
{
  lcd.init();
  lcd.createChar(0, co2_);
  lcd.createChar(1, up);
  lcd.createChar(2, pause);
  lcd.createChar(3, play);
  lcd.createChar(5, zakat);
  lcd.createChar(4, voshod);
  lcd.createChar(6, sun);
  lcd.createChar(7, moon_znak);
  /* lcd.createChar(0, bell);
    lcd.createChar(1, note);
    lcd.createChar(2, clock);
    lcd.createChar(3, heart);
    lcd.createChar(4, duck);
    lcd.createChar(5, check);
    lcd.createChar(6, cross);
    lcd.createChar(7, retarrow);*/
  // lcd.backlight();
  lcd.clear();
  // --------------------- СБРОС НАСТРОЕК ---------------------
  if (!digitalRead(SW)) {          // если нажат энкодер, сбросить настройки до 1


    EEPROM.write(2, 9);
    EEPROM.write(4, 0);
    EEPROM.write(6, 18);
    EEPROM.write(8, 0);
    ///
    EEPROM.put(10, 0.4);
    EEPROM.write(14, 25);
    ///
    EEPROM.put(25, 0.4);
    //
    EEPROM.write(35, 25);
    EEPROM.write(40, 0); //lcd on

    for (byte i = 0; i < 49; i++) {
      EEPROM.write(i * 2 + 100, 0);
    }
    for (byte i = 0; i < 49; i++) {
      EEPROM.write(i * 2 + 200, 0);
    }
    for (byte i = 0; i < 49; i++) {
      EEPROM.write(i * 2 + 300, 0);
    }
    for (byte i = 0; i < 49; i++) {
      EEPROM.write(i * 2 + 400, 0);
    }
    ///////rgb
    for (byte i = 0; i < 6; i++) {
      EEPROM.write(i + 510, 75);
    }

    /////////////////rgb motion
    EEPROM.write(530, 8);
    EEPROM.write(531, 5);

    EEPROM.write(532, 20);

    EEPROM.write(533, 19);
    EEPROM.write(534, 0);

    EEPROM.write(535, 30);

    /////////////////rgb motion

    ///moon motion
    EEPROM.write(540, 21);
    EEPROM.write(541, 0);
    EEPROM.write(542, 30);

    EEPROM.write(543, 23);
    EEPROM.write(544, 15);
    EEPROM.write(545, 20);

    EEPROM.write(550, 5);
    EEPROM.write(555, 5);
    ///moon motion

    EEPROM.write(560, 0);
    EEPROM.write(565, 5);
    EEPROM.write(570, 0);
    EEPROM.write(575, 5);
    EEPROM.write(580, 10);

    // day_pause
    EEPROM.write(582, 0);
    EEPROM.write(594, 0);
    EEPROM.write(596, 0);
    EEPROM.write(598, 0);

    lcd.setCursor(0, 0);
    lcd.print("Reset settings");
  }
  while (!digitalRead(SW));        // ждём отпускания кнопки
  lcd.clear();
  // очищаем дисплей, продолжаем работу
  ////////// --------------------- СБРОС НАСТРОЕК ---------------------

  co2[0] = EEPROM.read(2);
  co2[1] = EEPROM.read(4);
  co2[2] = EEPROM.read(6);
  co2[3] = EEPROM.read(8);

  EEPROM.get(10, delta_led);
  x_temp_led = EEPROM.read(14);

  EEPROM.get(25, delta);
  x_temp = EEPROM.read(35);

  lcd_on = EEPROM.read(40);

  for (byte i = 0; i < 49; i++) {
    EEPROM.get(i * 2 + 100, ch_1[i]);
  }
  for (byte i = 0; i < 49; i++) {
    EEPROM.get(i * 2 + 200, ch_2[i]);
  }
  for (byte i = 0; i < 49; i++) {
    EEPROM.get(i * 2 + 300, ch_3[i]);
  }
  for (byte i = 0; i < 49; i++) {
    EEPROM.get(i * 2 + 400, ch_4[i]);
  }

  for (byte i = 0; i < 6; i++) {
    rgb[i] = EEPROM.read(i + 510);
  }
  for (byte i = 0; i <= 6; i++) {
    rgb_motion[i] = EEPROM.read(i + 530);
  }

  for (byte i = 0; i <= 6; i++) {
    moon_motion[i] = EEPROM.read(i + 540);
  }

  pwm_moon_r = EEPROM.read(550);
  pwm_moon_b = EEPROM.read(555);

  food[0] = EEPROM.read(560);
  food[1] = EEPROM.read(565);
  food[2] = EEPROM.read(570);
  food[3] = EEPROM.read(575);
  food[4] = EEPROM.read(580);

  day_pause[0] = EEPROM.read(582);
  day_pause[1] = EEPROM.read(594);
  day_pause[2] = EEPROM.read(596);
  day_pause[3] = EEPROM.read(598);

  pinMode(ch_1_pin, OUTPUT);
  pinMode(ch_2_pin, OUTPUT);
  pinMode(ch_3_pin, OUTPUT);
  pinMode(ch_4_pin, OUTPUT);
  pinMode(co2_pin, OUTPUT);
  pinMode(cool_pin, OUTPUT);
  pinMode(food_pin, OUTPUT);
  pinMode(cool_led_pin, OUTPUT);

  pwm.begin();
  pwm.setPWMFreq(1600);  // This is the maximum PWM frequency

  // save I2C bitrate
  uint8_t twbrbackup = TWBR;
  // must be changed after calling Wire.begin() (inside pwm.begin())
  TWBR = 12; // upgrade to 400KHz!

  enc1.setType(TYPE2);
  attachInterrupt(0, isrCLK, CHANGE);    // прерывание на 2 пине! CLK у энка
  attachInterrupt(1, isrDT, CHANGE); // прерывание на 3 пине! DT у энка
  Wire.begin();

}
// функции с часами =======================================================================================================
byte decToBcd(byte val) {
  // Convert normal decimal numbers to binary coded decimal
  return ( (val / 10 * 16) + (val % 10) );
}

byte bcdToDec(byte val) {
  // Convert binary coded decimal to normal decimal numbers
  return ( (val / 16 * 10) + (val % 16) );
}

void setHour(byte hour1) {
  Wire.beginTransmission(DS3231_ADDRESS);
  Wire.write(0x02); //stop Oscillator
  Wire.write(decToBcd(hour1));
  Wire.endTransmission();
}

void setMinute(byte min1) {

  Wire.beginTransmission(DS3231_ADDRESS);
  Wire.write(0x01); //stop Oscillator
  Wire.write(decToBcd(min1));
  Wire.endTransmission();
}

void READ_TIME_RTC() {
  Wire.beginTransmission(DS3231_ADDRESS);        //104 is DS3231 device address
  Wire.write(0x00);                                  //Start at register 0
  Wire.endTransmission();
  Wire.requestFrom(DS3231_ADDRESS, 7);           //Request seven bytes
  if (Wire.available()) {
    s = Wire.read();                           //Get second
    m = Wire.read();                           //Get minute
    h   = Wire.read();                           //Get hour
    /* day     = Wire.read();
      date    = Wire.read();
      month   = Wire.read();                           //Get month
      year    = Wire.read();*/

    s = (((s & B11110000) >> 4) * 10 + (s & B00001111)); //Convert BCD to decimal
    m = (((m & B11110000) >> 4) * 10 + (m & B00001111));
    h   = (((h & B00110000) >> 4) * 10 + (h & B00001111));   //Convert BCD to decimal (assume 24 hour mode)
    /* day     = (day & B00000111); // 1-7
      date    = (((date & B00110000)>>4)*10 + (date & B00001111));         //Convert BCD to decimal  1-31
      month   = (((month & B00010000)>>4)*10 + (month & B00001111));       //msb7 is century overflow
      year    = (((year & B11110000)>>4)*10 + (year & B00001111));*/
    //  Serial.print(hour23);
  }
}
//
// ========================================================================================================================



void isrCLK() {
  enc1.tick();  // отработка в прерывании
}
void isrDT() {
  enc1.tick();  // отработка в прерывании
}

void LCD_Backlight() {

  if (lcd_on == 0) {
    if ((LR != 0) || (LG != 0) || (LB != 0) || (LW != 0) || (LC != 0)) {
      lcd.backlight();
    }
    else {

      if (flag == 1) {
        lcd_count++;
        lcd.backlight();
      }

      if (lcd_count == DELAY_MENU ) {
        lcd_count = 0;
        lcd.noBacklight();
        flag = 0;
      }
    }

  }
  if (lcd_on == 1) {
    if (flag == 1) {
      lcd_count++;
      lcd.backlight();
    }

    if (lcd_count == DELAY_MENU ) {
      lcd_count = 0;
      lcd.noBacklight();
      flag = 0;
    }
  }
  if (lcd_on == 2) {
    lcd.backlight();
  }
  /*
    if ((h > lcd_on[0]) && (h < lcd_on[1]))
    {
    lcd.backlight();
    }
    else
    lcd.noBacklight();

    if (lcd_on[0] > lcd_on[1])
    {
    lcd.backlight();
    }
    if (menu_val!=1){lcd.backlight();}
  */
}


void EEPROM_WR() {
  EEPROM.update(2, co2[0]);
  EEPROM.update(4, co2[1]);
  EEPROM.update(6, co2[2]);
  EEPROM.update(8, co2[3]);

  EEPROM.put(10, delta_led);
  EEPROM.update (14, x_temp_led);

  EEPROM.put(25, delta);
  EEPROM.update (35, x_temp);
  EEPROM.update (40, lcd_on);

  for (byte i = 0; i < 49; i++) {
    EEPROM.update(i * 2 + 100, ch_1[i]);
  }
  for (byte i = 0; i < 49; i++) {
    EEPROM.update(i * 2 + 200, ch_2[i]);
  }
  for (byte i = 0; i < 49; i++) {
    EEPROM.update(i * 2 + 300, ch_3[i]);
  }
  for (byte i = 0; i < 49; i++) {
    EEPROM.update(i * 2 + 400, ch_4[i]);
  }

  for (byte i = 0; i < 6; i++) {
    EEPROM.update(i + 510, rgb[i]);
  }
  for (byte i = 0; i < 7; i++) {
    EEPROM.update(i + 530, rgb_motion[i]);
  }
  for (byte i = 0; i < 7; i++) {
    EEPROM.update(i + 540, moon_motion[i]);
  }

  EEPROM.update(550, pwm_moon_r);
  EEPROM.update(555, pwm_moon_b);

  EEPROM.update(560, food[0]);
  EEPROM.update(565, food[1]);
  EEPROM.update(570, food[2]);
  EEPROM.update(575, food[3]);
  EEPROM.update(580, food[4]);

  EEPROM.update(582, day_pause[0]);
  EEPROM.update(594, day_pause[1]);
  EEPROM.update(596, day_pause[2]);
  EEPROM.update(598, day_pause[3]);
}

void loop() // основной программный цикл
{
  READ_TIME_RTC();
  dallRead(flagDallRead * 1000);
  dallRead_led(flagDallRead * 700);
  enc1.tick();
  MAIN();
  PWM_LED();
  LCD_Backlight();

}

